# CookingMaster

Disclaimer- Not that the current code quality is bad or anything but it can definitely be improved. However, I didn't want to spend too much time on a test. If I'm hired 
            you can expect better code quality than shown here. For instance, replacing the use of public variables by using properties (getters and setters), using async 
            functions instead of coroutines, avoiding the use static variables and singleton patterns, implementing new concepts such as scriptable objects, object pooling, 
            DOTS, among other.

Cooking Master - A Co-Op Food Delivery Game

» Added static top-down orthographic camera

» Added two players control:
                            For Player 1:
                                        Movement» (W, A, S, D), Spacebar for interacting with objects
                            For Player 2:
                                        Movement» (Up, Down, Left, Right), Right Control for interacting with objects

» Added 3 vegetables on each side (total 6) that can be picked up by either player and placed on their respective chopping board

» Added HUD draw for the vegetables that are picked

» Added HUD draw to show the current status of the chopping board

» Added trash can functionality which allows the player to throw any combination (salad) or vegetable(s) at hand

» Added the functionality to deliver items to the customer:
                            Delivering a correct item will increase the score,
                            while delivering a wrong item will result in a negative score,
                            both of the above will cause the customer to leave.

                            Note- The player that delivers the wrong item will only get the negative score.

» Individual timers for every customer, once the timer runs out the customer will leave resulting in a decrease of score for both players.

» Power Ups:
            If the player delivers within the first 20 seconds from when the customer arrives, the customer awards the player with a power up:
                            Speed:  Adds +1 to the player's current speed.
                            Time:   Adds +60 to the player's current time.
                            Score:  Adds +10 to the player's current score.
                            
» Added a reset option to restart the game.