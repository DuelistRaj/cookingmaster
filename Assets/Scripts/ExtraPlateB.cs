﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtraPlateB : MonoBehaviour
{
    private bool inRangeB;
    private bool isFull;

    private string itemOnPlate;

    // Update is called once per frame
    void Update()
    {
        if (inRangeB && Input.GetKeyDown(KeyCode.RightControl) && PlaceItemB.onHandCombination == null)
        {
            if (!isFull && PickUp.pickUpCountB > 0)
            {
                PlaceItemOnPlate();
            }
            else if (isFull && PickUp.pickUpCountB < 2)
            {
                PickUpItemFromPlate();
            }
        }
    }

    /// <summary>
    /// Place vegetable on the extra plate
    /// </summary>
    private void PlaceItemOnPlate()
    {
        isFull = true;
        PickUp.pickUpCountB--;
        PickUpText.Instance.extraPlateB.text = itemOnPlate = PickUp.pickedUpItemB[PickUp.pickUpCountB];
        PickUpText.Instance.itemsHUDB[PickUp.pickUpCountB].text = null;
    }

    /// <summary>
    /// Pick up vegetable from the extra plate
    /// </summary>
    private void PickUpItemFromPlate()
    {
        isFull = false;
        PickUp.pickedUpItemB[PickUp.pickUpCountB] = PickUpText.Instance.itemsHUDB[PickUp.pickUpCountB].text = itemOnPlate;
        PickUpText.Instance.extraPlateB.text = null;
        PickUp.pickUpCountB++;
    }


    /// <summary>
    /// Checks if the player has entered the range
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name.Equals("Player B"))
        {
            inRangeB = true;
        }
    }

    /// <summary>
    /// Checks if the player has exited the range
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.name.Equals("Player B"))
        {
            inRangeB = false;
        }
    }
}
