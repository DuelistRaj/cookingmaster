﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class PlaceItemA : MonoBehaviour
{
    public static bool frozenA;

    private bool inRangeA;
    private bool isChopping;

    private int itemsChopped;

    private string currentCombination;

    public static string onHandCombination;

    void Start()
    {
        frozenA = false;
        onHandCombination = null;
    }

    // Update is called once per frame
    void Update()
    {
        //Checks if Player A is in range and has at least 1 item picked up
        //If so then the item is placed when the correct key is pressed
        if (inRangeA && Input.GetKeyDown(KeyCode.Space) && !isChopping)
        {
            if(itemsChopped < 3 && PickUp.pickUpCountA > 0)
            {
                CreateItemCombination();
                ItemChop();
            }
            else if(itemsChopped >= 3 && PickUp.pickUpCountA == 0)
            {
                PickUpItemCombination();
            }
        }
    }

    /// <summary>
    /// Creates the vegetable salad combination
    /// </summary>
    private void CreateItemCombination()
    {
        isChopping = true;
        itemsChopped++;
        PickUp.pickUpCountA--;
        currentCombination += PickUp.pickedUpItemA[PickUp.pickUpCountA];
        PickUpText.Instance.currentStatusA.text = "Chopping " + PickUp.pickedUpItemA[PickUp.pickUpCountA] + "!";
        PickUp.pickedUpItemA[PickUp.pickUpCountA] = PickUpText.Instance.itemsHUDA[PickUp.pickUpCountA].text  = null;
        frozenA = true;
    }
    
    /// <summary>
    /// Pick up the vegetable salad combination from the chopping board
    /// </summary>
    private void PickUpItemCombination()
    {
        PickUpText.Instance.currentStatusA.text = "Picked up " + currentCombination + "!";
        onHandCombination = currentCombination;
        currentCombination = null;
        itemsChopped = 0;
    }

    /// <summary>
    /// Time required to chop the vegetables
    /// </summary>
    private async void ItemChop()
    {
        await Task.Delay(1000);
        PickUpText.Instance.currentStatusA.text = "Chopped!";
        frozenA = false;
        isChopping = false;
        if(itemsChopped == 3)
        {
            PickUpText.Instance.currentStatusA.text = "Ready to be delivered!";
        }
    }

    /// <summary>
    /// Checks if the player has entered the range
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name.Equals("Player A"))
        {
            inRangeA = true;
        }
    }

    /// <summary>
    /// Checks if the player has exited the range
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.name.Equals("Player A"))
        {
            inRangeA = false;
        }
    }
}
