﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickUp : MonoBehaviour
{
    private bool inRangeA;
    private bool inRangeB;

    public static int pickUpCountA;
    public static int pickUpCountB;

    public static string[] pickedUpItemA = new string[2];
    public static string[] pickedUpItemB = new string[2];

    void Start()
    {
        pickUpCountA = 0;
        pickUpCountB = 0;
    }

    // Update is called once per frame
    void Update()
    {
        //Checks if Player A is in range of an item and has less than 2 items picked up
        //If so then the item is picked up when the correct key is pressed
        if(inRangeA && Input.GetKeyDown(KeyCode.Space) && pickUpCountA < 2 && PlaceItemA.onHandCombination == null)
        {
            PickUpItemA();

            //Reversing the array to maintain the order in which the items were picked
            if(pickUpCountA == 2)
            {
                System.Array.Reverse(pickedUpItemA);
                System.Array.Reverse(PickUpText.Instance.itemsHUDA);
            }
        }

        //Checks if Player B is in range of an item and has less than 2 items picked up
        //If so then the item is picked up when the correct key is pressed
        if (inRangeB && Input.GetKeyDown(KeyCode.RightControl) && pickUpCountB < 2 && PlaceItemB.onHandCombination == null)
        {
            PickUpItemB();

            //Reversing the array to maintain the order in which the items were picked
            if (pickUpCountB == 2)
            {
                System.Array.Reverse(pickedUpItemB);
                System.Array.Reverse(PickUpText.Instance.itemsHUDB);
            }
        }
    }

    /// <summary>
    /// Item pick by A
    /// </summary>
    private void PickUpItemA()
    {
        pickedUpItemA[pickUpCountA] = gameObject.name;
        PickUpText.Instance.itemsHUDA[pickUpCountA].text = pickedUpItemA[pickUpCountA];
        pickUpCountA++;
    }

    /// <summary>
    /// Item pick by B
    /// </summary>
    private void PickUpItemB()
    {
        pickedUpItemB[pickUpCountB] = gameObject.name;
        PickUpText.Instance.itemsHUDB[pickUpCountB].text = pickedUpItemB[pickUpCountB];
        pickUpCountB++;
    }

    /// <summary>
    /// Checks if the player has entered the range
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.name.Equals("Player A"))
        {
            inRangeA = true;
        }

        if (collision.gameObject.name.Equals("Player B"))
        {
            inRangeB = true;
        }
    }

    /// <summary>
    /// Checks if the player has exited the range
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.name.Equals("Player A"))
        {
            inRangeA = false;
        }

        if (collision.gameObject.name.Equals("Player B"))
        {
            inRangeB = false;
        }
    }
}
