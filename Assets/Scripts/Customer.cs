﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class Customer : MonoBehaviour
{
    private bool inRangeA;
    private bool inRangeB;

    private string[] ingredients = { "Onion", "Corn", "Peas", "Lentil", "Chili", "Beans" };
    private string orderCombination;

    private Text orderCombinationText;

    private float waitTime = 30.0f;
    private float powerUpTimer = 20.0f;

    public static int scoreA;
    public static int scoreB;

    private int powerUpAssign;

    // Start is called before the first frame update
    void Start()
    {
        scoreA = 0;
        scoreB = 0;
        NewCustomer();
        StartCoroutine(TimeUp());
    }

    // Update is called once per frame
    void Update()
    {
        powerUpTimer -= Time.deltaTime;
        if (inRangeA && Input.GetKeyDown(KeyCode.Space) && PlaceItemA.onHandCombination != null)
        {
            if(orderCombination.Equals(PlaceItemA.onHandCombination))
            {
                PowerUpAssignA();
                CorrectItemDeliveredA();
                NewCustomer();
                StopAllCoroutines();
                StartCoroutine(TimeUp());
            }
            else
            {
                WrongItemDeliveredA();
                NewCustomer();
                StopAllCoroutines();
                StartCoroutine(TimeUp());
            }
        }

        if (inRangeB && Input.GetKeyDown(KeyCode.RightControl) && PlaceItemB.onHandCombination != null)
        {
            if (orderCombination.Equals(PlaceItemB.onHandCombination))
            {
                PowerUpAssignB();
                CorrectItemDeliveredB();
                NewCustomer();
                StopAllCoroutines();
                StartCoroutine(TimeUp());
            }
            else
            {
                WrongItemDeliveredB();
                NewCustomer();
                StopAllCoroutines();
                StartCoroutine(TimeUp());
            }
        }
    }

    /// <summary>
    /// Manages the timer of each customer
    /// </summary>
    /// <returns></returns>
    private IEnumerator TimeUp()
    {
        yield return new WaitForSeconds(waitTime);
        scoreA--;
        PickUpText.Instance.ScoreA.text = "Score: " + scoreA.ToString();
        scoreB--;
        PickUpText.Instance.ScoreB.text = "Score: " + scoreB.ToString();
        NewCustomer();
        StartCoroutine(TimeUp());
    }

    /// <summary>
    /// Creates a new customer
    /// </summary>
    private void NewCustomer()
    {
        orderCombination = null;
        for (int i = 0; i < 3; i++)
        {
            orderCombination += ingredients[Random.Range(0, 6)];
        }
        orderCombinationText = GetComponentInChildren<Text>();
        orderCombinationText.text = orderCombination;
        powerUpTimer = 20.0f;
    }

    /// <summary>
    /// Assigns the powers ups to Player A
    /// </summary>
    private void PowerUpAssignA()
    {
        if (powerUpTimer > 0)
        {
            powerUpAssign = Random.Range(0, 3);
            switch (powerUpAssign)
            {
                case 0:
                    scoreA += 10;
                    PickUpText.Instance.PowerUpA.text = "Score Boost!";
                    break;
                case 1:
                    PlayerTimer.timerA += 60;
                    PickUpText.Instance.PowerUpA.text = "Time Added!";
                    break;
                case 2:
                    PlayerMovementA.movementSpeed += 1;
                    PickUpText.Instance.PowerUpA.text = "Extra Speed!";
                    break;
            }
        }
    }

    /// <summary>
    /// Assigns the powers ups to Player B
    /// </summary>
    private void PowerUpAssignB()
    {
        if (powerUpTimer > 0)
        {
            powerUpAssign = Random.Range(0, 3);
            switch (powerUpAssign)
            {
                case 0:
                    scoreB += 10;
                    PickUpText.Instance.PowerUpB.text = "Score Boost!";
                    break;
                case 1:
                    PlayerTimer.timerB += 60;
                    PickUpText.Instance.PowerUpB.text = "Time Added!";
                    break;
                case 2:
                    PlayerMovementB.movementSpeed += 1;
                    PickUpText.Instance.PowerUpB.text = "Extra Speed!";
                    break;
            }
        }
    }

    /// <summary>
    /// Checks if A delivered the correct item
    /// </summary>
    private void CorrectItemDeliveredA()
    {
        PickUpText.Instance.currentStatusA.text = "Delivered " + PlaceItemA.onHandCombination + " to " + gameObject.name + "! ";
        PlaceItemA.onHandCombination = null;
        scoreA += 5;
        PickUpText.Instance.ScoreA.text = "Score: " + scoreA.ToString();
    }

    /// <summary>
    /// Checks if B delivered the correct item
    /// </summary>
    private void CorrectItemDeliveredB()
    {
        PickUpText.Instance.currentStatusB.text = "Delivered " + PlaceItemB.onHandCombination + " to " + gameObject.name + "! ";
        PlaceItemB.onHandCombination = null;
        scoreB += 5; ;
        PickUpText.Instance.ScoreB.text = "Score: " + scoreB.ToString();
    }

    /// <summary>
    /// Checks if A delivered the wrong item
    /// </summary>
    private void WrongItemDeliveredA()
    {
        PickUpText.Instance.currentStatusA.text = "Wrong Item delivered!";
        PlaceItemA.onHandCombination = null;
        scoreA--;
        PickUpText.Instance.ScoreA.text = "Score: " + scoreA.ToString();
    }

    /// <summary>
    /// Checks if B delivered the wrong item
    /// </summary>
    private void WrongItemDeliveredB()
    {
        PickUpText.Instance.currentStatusB.text = "Wrong Item delivered!";
        PlaceItemB.onHandCombination = null;
        scoreB--;
        PickUpText.Instance.ScoreB.text = "Score: " + scoreB.ToString();
    }

    /// <summary>
    /// Checks if the player has entered the range
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name.Equals("Player A"))
        {
            inRangeA = true;
        }

        if (collision.gameObject.name.Equals("Player B"))
        {
            inRangeB = true;
        }
    }

    /// <summary>
    /// Checks if the player has exited the range
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.name.Equals("Player A"))
        {
            inRangeA = false;
        }

        if (collision.gameObject.name.Equals("Player B"))
        {
            inRangeB = false;
        }
    }
}
