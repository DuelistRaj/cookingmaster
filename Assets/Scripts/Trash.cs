﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trash : MonoBehaviour
{

    private bool inRangeA;
    private bool inRangeB;

    // Update is called once per frame
    void Update()
    {
        if (inRangeA && Input.GetKeyDown(KeyCode.Space))
        {
            TrashItemsOnHandA();
            TrashCombinationOnHandA();
        }

        if (inRangeB && Input.GetKeyDown(KeyCode.RightControl))
        {
            TrashItemsOnHandB();
            TrashCombinationOnHandB();
        }
    }

    /// <summary>
    /// Logic to put vegetables into trash for Player A
    /// </summary>
    private void TrashItemsOnHandA()
    {
        if (PickUp.pickUpCountA > 0)
        {
            PickUp.pickUpCountA = 0;
            PickUpText.Instance.currentStatusA.text = "Trashed items on hand!";
            PickUpText.Instance.itemsHUDA[0].text = null;
            PickUpText.Instance.itemsHUDA[1].text = null;
        }
    }

    /// <summary>
    /// Logic to put salad into trash for Player A
    /// </summary>
    private void TrashCombinationOnHandA()
    {
        if (PlaceItemA.onHandCombination != null)
        {
            PickUpText.Instance.currentStatusA.text = "Trashed " + PlaceItemA.onHandCombination + "!";
            PlaceItemA.onHandCombination = null;
        }
    }

    /// <summary>
    /// Logic to put vegetables into trash for Player B
    /// </summary>
    private void TrashItemsOnHandB()
    {
        if (PickUp.pickUpCountB > 0)
        {
            PickUp.pickUpCountB = 0;
            PickUpText.Instance.currentStatusB.text = "Trashed items on hand!";
            PickUpText.Instance.itemsHUDB[0].text = null;
            PickUpText.Instance.itemsHUDB[1].text = null;
        }
    }

    /// <summary>
    /// Logic to put salad into trash for Player B
    /// </summary>
    private void TrashCombinationOnHandB()
    {
        if (PlaceItemB.onHandCombination != null)
        {
            PickUpText.Instance.currentStatusB.text = "Trashed " + PlaceItemB.onHandCombination + "!";
            PlaceItemB.onHandCombination = null;
        }
    }

    /// <summary>
    /// Checks if the player has entered the range
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name.Equals("Player A"))
        {
            inRangeA = true;
        }

        if (collision.gameObject.name.Equals("Player B"))
        {
            inRangeB = true;
        }
    }

    /// <summary>
    /// Checks if the player has exited the range
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.name.Equals("Player A"))
        {
            inRangeA = false;
        }

        if (collision.gameObject.name.Equals("Player B"))
        {
            inRangeB = false;
        }
    }
}
