﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtraPlateA : MonoBehaviour
{
    private bool inRangeA;
    private bool isFull;

    private string itemOnPlate;

    // Update is called once per frame
    void Update()
    {
        if(inRangeA && Input.GetKeyDown(KeyCode.Space) && PlaceItemA.onHandCombination == null)
        {
            if(!isFull && PickUp.pickUpCountA > 0)
            {
                PlaceItemOnPlate();
            }
            else if(isFull && PickUp.pickUpCountA < 2)
            {
                PickUpItemFromPlate();
            }
        }
    }

    /// <summary>
    /// Place vegetable on the extra plate
    /// </summary>
    private void PlaceItemOnPlate()
    {
        isFull = true;
        PickUp.pickUpCountA--;
        PickUpText.Instance.extraPlateA.text = itemOnPlate = PickUp.pickedUpItemA[PickUp.pickUpCountA];
        PickUpText.Instance.itemsHUDA[PickUp.pickUpCountA].text = null;
    }

    /// <summary>
    /// Pick up vegetable from the extra plate
    /// </summary>
    private void PickUpItemFromPlate()
    {
        isFull = false;
        PickUp.pickedUpItemA[PickUp.pickUpCountA] = PickUpText.Instance.itemsHUDA[PickUp.pickUpCountA].text = itemOnPlate;
        PickUpText.Instance.extraPlateA.text = null;
        PickUp.pickUpCountA++;
    }

    /// <summary>
    /// Checks if the player has entered the range
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name.Equals("Player A"))
        {
            inRangeA = true;
        }
    }

    /// <summary>
    /// Checks if the player has exited the range
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.name.Equals("Player A"))
        {
            inRangeA = false;
        }
    }
}
