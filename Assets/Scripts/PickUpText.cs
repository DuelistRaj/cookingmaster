﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickUpText : MonoBehaviour
{

    public Text[] itemsHUDA = new Text[2];
    public Text[] itemsHUDB = new Text[2];

    public Text currentStatusA;
    public Text currentStatusB;

    public Text extraPlateA;
    public Text extraPlateB;

    public Text TimerA;
    public Text TimerB;

    public Text ScoreA;
    public Text ScoreB;

    public Text PlayerWon;

    public Text PowerUpA;
    public Text PowerUpB;

    private static PickUpText _instance;

    public static PickUpText Instance
    {
        get
        {
            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
}
