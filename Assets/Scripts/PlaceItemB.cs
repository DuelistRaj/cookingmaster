﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class PlaceItemB : MonoBehaviour
{
    public static bool frozenB;

    private bool inRangeB;
    private bool isChopping;

    private int itemsChopped;

    private string currentCombination;

    public static string onHandCombination;

    void Start()
    {
        frozenB = false;
        onHandCombination = null;
    }

    // Update is called once per frame
    void Update()
    {
        //Checks if Player B is in range and has at least 1 item picked up
        //If so then the item is placed when the correct key is pressed
        if (inRangeB && Input.GetKeyDown(KeyCode.RightControl) && !isChopping)
        {
            if (itemsChopped < 3 && PickUp.pickUpCountB > 0)
            {
                CreateItemCombination();
                ItemChop();
            }
            else if (itemsChopped >= 3 && PickUp.pickUpCountB == 0)
            {
                PickUpItemCombination();
            }
        }
    }

    /// <summary>
    /// Creates the vegetable salad combination
    /// </summary>
    private void CreateItemCombination()
    {
        isChopping = true;
        itemsChopped++;
        PickUp.pickUpCountB--;
        currentCombination += PickUp.pickedUpItemB[PickUp.pickUpCountB];
        PickUpText.Instance.currentStatusB.text = "Chopping " + PickUp.pickedUpItemB[PickUp.pickUpCountB] + "!";
        PickUp.pickedUpItemB[PickUp.pickUpCountB] = PickUpText.Instance.itemsHUDB[PickUp.pickUpCountB].text = null;
        frozenB = true;
    }

    /// <summary>
    /// Pick up the vegetable salad combination from the chopping board
    /// </summary>
    private void PickUpItemCombination()
    {
        PickUpText.Instance.currentStatusB.text = "Picked up " + currentCombination + "!";
        onHandCombination = currentCombination;
        currentCombination = null;
        itemsChopped = 0;
    }

    /// <summary>
    /// Time required to chop the vegetables
    /// </summary>
    private async void ItemChop()
    {
            await Task.Delay(1000);
            PickUpText.Instance.currentStatusB.text = "Chopped!";
            frozenB = false;
            isChopping = false;
            if (itemsChopped == 3)
            {
                PickUpText.Instance.currentStatusB.text = "Ready to be delivered!";
            }
        }

    /// <summary>
    /// Checks if the player has entered the range
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name.Equals("Player B"))
        {
            inRangeB = true;
        }
    }

    /// <summary>
    /// Checks if the player has exited the range
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerExit2D(Collider2D collision)
    { 
        if (collision.gameObject.name.Equals("Player B"))
        {
            inRangeB = false;
        }
    }
}
