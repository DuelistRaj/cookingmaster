﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.SceneManagement;
using System.Threading;

public class PlayerTimer : MonoBehaviour
{
    public static float timerA;
    public static float timerB;

    [SerializeField] private GameObject panel;

    void Start()
    {
        panel.SetActive(false);
        timerA = 300;
        timerB = 300;
    }

    // Update is called once per frame
    void Update()
    {

        TimerPlayerA();
        TimerPlayerB();

        GameOver();
    }

    /// <summary>
    /// Timer for Player A
    /// </summary>
    private void TimerPlayerA()
    {
        if (timerA > 0)
        {
            timerA -= Time.deltaTime;
            PickUpText.Instance.TimerA.text = "Timer: " + ((int)timerA).ToString();
        }
        else
        {
            PlaceItemA.frozenA = true;
        }
    }

    /// <summary>
    /// Timer for Player B
    /// </summary>
    private void TimerPlayerB()
    {
        if (timerB > 0)
        {
            timerB -= Time.deltaTime;
            PickUpText.Instance.TimerB.text = "Timer: " + ((int)timerB).ToString();
        }
        else
        {
            PlaceItemB.frozenB = true;
        }
    }

    /// <summary>
    /// Game Over Logic
    /// </summary>
    private void GameOver()
    {
        if (timerA <= 0 && timerB <= 0)
        {
            panel.SetActive(true);
            if (Customer.scoreA > Customer.scoreB)
            {
                PickUpText.Instance.PlayerWon.text = "Player A Won with Score: " + Customer.scoreA.ToString();
            }
            else if (Customer.scoreA < Customer.scoreB)
            {
                PickUpText.Instance.PlayerWon.text = "Player B Won with Score: " + Customer.scoreB.ToString();
            }
            else
            {
                PickUpText.Instance.PlayerWon.text = "It's a tie!";
            }
        }

    }

    /// <summary>
    /// Restarts the level
    /// </summary>
    public void Reset()
    {
        SceneManager.LoadScene(0);
    }
}
