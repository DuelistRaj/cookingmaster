﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementA : MonoBehaviour
{
    private Rigidbody2D rb;

    private Vector2 movementDirection;

    public static float movementSpeed;

    // Start is called before the first frame update
    void Start()
    {
        movementSpeed = 5.0f;
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        ProcessInput();
    }

    /// <summary>
    /// Physics Calculation
    /// </summary>
    void FixedUpdate()
    {
        Movement();
    }

    /// <summary>
    /// To take input using Unity's Input Manager (Player 2)
    /// </summary>
    private void ProcessInput()
    {
        if(!PlaceItemA.frozenA)
        {
            float moveX = Input.GetAxisRaw("P1_Horizontal");
            float moveY = Input.GetAxisRaw("P1_Vertical");

            movementDirection = new Vector2(moveX, moveY).normalized;
        }
    }

    /// <summary>
    /// Change velocity to generate player movement
    /// </summary>
    private void Movement()
    {
        if (!PlaceItemA.frozenA)
            rb.velocity = new Vector2(movementDirection.x * movementSpeed, movementDirection.y * movementSpeed);
        else
            rb.velocity = Vector2.zero;
    }
}
